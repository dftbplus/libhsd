module test_elements_help
  use libhsd
  implicit none

contains

  subroutine test()

    class(Element), pointer :: root, child1, child2

    call create(root, "root")
    call create(child1, "child1")
    call root%appendChild(child1)
    call create(child2, "child11")
    call child1%appendChild(child2)
    call create(child1, "child2")
    call root%appendChild(child1)
    call create(child2, "child21")
    call child1%appendChild(child2)
    call dumpTree(root)
    call destroy(root)

  end subroutine test

end module test_elements_help

  
program test_elements
  use  test_elements_help
  implicit none

  call test()
  
end program test_elements
  
