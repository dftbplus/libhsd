program test_bintree
  use libhsd
  implicit none

  class(Element), pointer :: root, child1, child2
  type(BinTreeManager) :: manager
  type(BinTreePrinter) :: printer
  type(AttribDict), target :: attribs
  

  call create(root, "root")
  call init(attribs)
  call attribs%set(":hsd:id", "szerencse")
  call manager%setChildContent(root, "luckynum", 13, &
      & attribs=AttribDictView(attribs))
  call printer%process(root)
  call destroy(root)

end program test_bintree
  
