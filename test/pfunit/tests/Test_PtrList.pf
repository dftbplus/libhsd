module Test_PtrList
  use libhsd
  use pfunit_mod
  implicit none

  public :: TestPtrList

  type, extends(BaseItem) :: IntPtr
    integer, pointer :: ptr
  contains
    procedure :: isEqualTo => IntPtr_isEqualTo
  end type IntPtr


@TestCase
  type, extends(TestCase) :: TestPtrList
    type(IntPtr), pointer :: items(:)
  contains
    procedure :: setUp
    procedure :: tearDown
    procedure :: equivalent
    procedure :: appendElements
  end type TestPtrList

  integer, parameter :: MAX_LIST_SIZE = 20

contains

  function IntPtr_isEqualTo(this, other) result(isEqual)
    class(IntPtr), intent(in) :: this
    class(BaseItem), intent(in) :: other
    logical :: isEqual

    select type (other)
    class is (IntPtr)
      isEqual = associated(this%ptr, other%ptr)
    class default
      isEqual = .false.
    end select

  end function IntPtr_isEqualTo


  subroutine setUp(this)
    class(TestPtrList), intent(inout) :: this

    integer :: ii
    
    allocate(this%items(MAX_LIST_SIZE))
    do ii = 1, MAX_LIST_SIZE
      allocate(this%items(ii)%ptr)
      this%items(ii)%ptr = ii
    end do
    
  end subroutine setUp


  subroutine tearDown(this)
    class(TestPtrList), intent(inout) :: this

    integer :: ii

    do ii = 1, size(this%items)
      deallocate(this%items(ii)%ptr)
    end do

  end subroutine tearDown


  function equivalent(this, list, values)
    class(TestPtrList), intent(in) :: this
    type(PtrList), intent(inout) :: list
    integer, intent(in) :: values(:)
    logical :: equivalent

    type(PtrList) :: myList

    call init(myList)
    call this%appendElements(myList, values)
    equivalent = (myList == list)

  end function equivalent


  subroutine appendElements(this, list, elems)
    class(TestPtrList), intent(in) :: this
    type(PtrList), intent(inout) :: list
    integer, intent(in) :: elems(:)
    
    type(ItemPtr) :: wrapper
    integer :: ii

    do ii = 1, size(elems)
      wrapper%ptr => this%items(elems(ii))
      call list%append(wrapper)
    end do

  end subroutine appendElements
  

@Test  
  subroutine testGetSize(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    @assertEqual(4, list%getSize())

  end subroutine testGetSize


@Test  
  subroutine testAppend(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    @assertTrue(this%equivalent(list, [ 1, 2, 3, 4 ]))

  end subroutine testAppend



@Test  
  subroutine testInsert(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
    type(ItemPtr) :: wrapper

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])

    ! Beginning of the list
    wrapper%ptr => this%items(11)
    call list%insert(1, wrapper)
    @assertTrue(this%equivalent(list, [ 11, 1, 2, 3, 4 ]))

    ! Somewhere in the list
    wrapper%ptr => this%items(13)
    call list%insert(3, wrapper)
    @assertTrue(this%equivalent(list, [ 11, 1, 13, 2, 3, 4 ]))

    ! End of the list
    wrapper%ptr => this%items(17)
    call list%insert(7, wrapper)
    @assertTrue(this%equivalent(list, [ 11, 1, 13, 2, 3, 4, 17 ]))

  end subroutine testInsert


@Test  
  subroutine testGet(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
    type(ItemPtr) :: pItem

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    call list%get(1, pItem)
    @assertTrue(associated(pItem%ptr, this%items(1)))
    call list%get(2, pItem)
    @assertTrue(associated(pItem%ptr, this%items(2)))
    call list%get(4, pItem)
    @assertTrue(associated(pItem%ptr, this%items(4)))

  end subroutine testGet
    

@Test
  subroutine testSet(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
    type(ItemPtr) :: ptr, ptr2
  
    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    ptr%ptr => this%items(12)
    call list%set(2, ptr, ptr2)
    @assertTrue(this%equivalent(list, [ 1, 12, 3, 4 ]))
    select type (pp => ptr2%ptr)
    type is (IntPtr)
      @assertTrue(pp%ptr == 2, "Old value corrupted")
    class default
      @assertTrue(.false., "Wrong type data type")
    end select

  end subroutine testSet
  

@Test
  subroutine testDelete(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
  
    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    call list%delete(3)
    @assertTrue(this%equivalent(list, [ 1, 2, 4 ]))

  end subroutine testDelete

  
@Test
  subroutine testPop(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
    type(ItemPtr) :: ptr
  
    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    call list%pop(2, ptr)
    @assertTrue(this%equivalent(list, [ 1, 3, 4 ]))
    select type (pp => ptr%ptr)
    type is (IntPtr)
      @assertTrue(pp%ptr == 2, "Old value corrupted")
    class default
      @assertTrue(.false., "Wrong type data type")
    end select

  end subroutine testPop


@Test  
  subroutine testExtend(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list1, list2
  
    call init(list1)
    call init(list2)
    call this%appendElements(list1, [ 1, 2, 3, 4 ])
    call this%appendElements(list2, [ 11, 12, 13, 14 ])
    call list1%extend(list2)
    @assertTrue(this%equivalent(list1, [ 1, 2, 3, 4, 11, 12, 13, 14 ]))
    @assertTrue(this%equivalent(list2, [ 11, 12, 13, 14 ]))

  end subroutine testExtend


@Test  
  subroutine testEqualNotEqual1(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list1, list2

    call init(list1)
    call init(list2)
    call this%appendElements(list1, [ 1, 2, 3, 4 ])
    call this%appendElements(list2, [ 1, 2, 3, 4 ])
    @assertTrue(list1 == list2)
    @assertFalse(list1 /= list2)

  end subroutine testEqualNotEqual1


@Test  
  subroutine testEqualNotEqual2(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list1, list2

    call init(list1)
    call init(list2)
    call this%appendElements(list1, [ 1, 2, 3, 4 ])
    call this%appendElements(list2, [ 11, 12, 13, 14 ])
    @assertTrue(list1 /= list2)
    @assertFalse(list1 == list2)

  end subroutine testEqualNotEqual2


@Test  
  subroutine testGetPosition(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList) :: list
    type(ItemPtr) :: pItem

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    pItem%ptr => this%items(1)
    @assertTrue(list%getPosition(pItem) == 1)
    pItem%ptr => this%items(2)
    @assertTrue(list%getPosition(pItem) == 2)
    pItem%ptr => this%items(4)
    @assertTrue(list%getPosition(pItem) == 4)
    pItem%ptr => this%items(5)
    @assertTrue(list%getPosition(pItem) == 0)

  end subroutine testGetPosition


@Test  
  subroutine testAssignmentToPristine(this)
    class(TestPtrList), intent(inout) :: this
    
    type(PtrList), allocatable :: list1, list2

    allocate(list1)
    allocate(list2)
    call init(list1)
    call init(list2)
    call this%appendElements(list1, [ 1, 2, 3, 4 ])
    list2 = list1
    @assertTrue(this%equivalent(list1, [ 1, 2, 3, 4 ]))
    @assertTrue(this%equivalent(list2, [ 1, 2, 3, 4 ]))
    deallocate(list1)
    @assertTrue(this%equivalent(list2, [ 1, 2, 3, 4 ]))

  end subroutine testAssignmentToPristine


@Test  
  subroutine testAssignmentToFilled(this)
    class(TestPtrList), intent(inout) :: this
    
    type(PtrList), allocatable :: list1, list2

    allocate(list1)
    allocate(list2)
    call init(list1)
    call init(list2)
    call this%appendElements(list1, [ 1, 2, 3, 4 ])
    call this%appendElements(list2, [ 11, 12, 13, 14 ])
    list1 = list2
    @assertTrue(this%equivalent(list1, [ 11, 12, 13, 14 ]))
    @assertTrue(this%equivalent(list2, [ 11, 12, 13, 14 ]))
    deallocate(list2)
    @assertTrue(this%equivalent(list1, [ 11, 12, 13, 14 ]))
    
  end subroutine testAssignmentToFilled


@Test  
  subroutine testPtrViewGetSize(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList), target :: list
    type(PtrListView) :: view

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    view = PtrListView(list)
    @assertEqual(4, view%getSize())

  end subroutine testPtrViewGetSize


@Test  
  subroutine testPtrViewGet(this)
    class(TestPtrList), intent(inout) :: this

    type(PtrList), target :: list
    type(PtrListView) :: view
    type(ItemPtr) :: pItem

    call init(list)
    call this%appendElements(list, [ 1, 2, 3, 4 ])
    view = PtrListView(list)
    call view%get(1, pItem)
    @assertTrue(associated(pItem%ptr, this%items(1)))
    call view%get(2, pItem)
    @assertTrue(associated(pItem%ptr, this%items(2)))
    call view%get(4, pItem)
    @assertTrue(associated(pItem%ptr, this%items(4)))

  end subroutine testPtrViewGet

end module Test_PtrList
