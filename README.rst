libhsd
======

The libhsd library contains contains following components:

* generic, easily extendible collections (e.g. list with arbitrary item type,
  dictionary with arbitrary key and item type, etc.)

* the parsing and tree building utilities for the HSD (human-readable structured
  data) format

* communication routines enabling a seamless data exchange between an embedded
  Fortran code and its C or Fortran host without having to write complicated
  APIs.

The library is available under the **simplified BSD license**.
