#include <stdlib.h>

long libhsd_strhash(register unsigned char *ptr, int size)
{
  register int len;
  register long res;

  len = size;
  res = *ptr << 7;
  while (--len >= 0) {
    res = (1000003 * res) ^ *ptr++;
  }
  res ^= size;
  return res;
}


void libhsd_nextslotindex(unsigned long *ind, unsigned long *perturb)
{
  const unsigned long PERTURB_SHIFT = 5;

  *ind = ((*ind) << 2) + (*ind) + (*perturb) + 1;
  *perturb >>= PERTURB_SHIFT;
}
