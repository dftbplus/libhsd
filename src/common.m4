dnl Undefining some M4 builtins to avoid conflicts with Fortran code
dnl invoke them via the builtin() command if needed.
dnl
undefine(`len')dnl
undefine(`index')dnl
undefine(`shift')dnl

dnl Set DEBUG to 0 (production mode), if not explicitely defined otherwise
dnl keep original value.
dnl
define(`DEBUG', ifdef(`DEBUG', DEBUG, 0))

dnl Indicates debug code.
dnl $1 Code. Only included, if DEBUG > 0
dnl
define(`_debug', ifelse(eval(DEBUG` > 0'), 1, $1))

dnl Removing directory part of a file
dnl
define(`basename', `patsubst($1,`.*/',`')')

dnl Assertion
dnl $1  Condition to check (only inserted if in debug mode).
dnl
define(`_assert', _debug(`dnl
if (.not. ($1)) then
  call assertFailed("`basename(__file__)'", &
      & `__line__')
end if'))

dnl Internal error
dnl $1  Message to print
define(`_internal_error', dnl
`call internalError($1, &
    & "basename(__file__)", &
    & __line__)')
