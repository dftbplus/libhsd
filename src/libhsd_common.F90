module libhsd_common
  use, intrinsic :: iso_c_binding
  use, intrinsic :: iso_fortran_env
  implicit none
  public

  integer, parameter :: szk = c_size_t

  !> 64 bit integer
  integer, parameter :: long = selected_int_kind(18)

  integer, parameter :: stdout = output_unit
  integer, parameter :: stderr = error_unit
  integer, parameter :: stdin = input_unit

contains

  !> Stops code signalizing failed assertion.
  !!
  subroutine assertFailed(file, line)
    character(*), intent(in) :: file
    integer, intent(in) :: line

    write(stderr, "(A)") "!!! ASSERTION FAILED"
    write(stderr, "(A,1X,A)") "File:", file
    write(stderr, "(A,1X,I0)") "Line:", line
    stop 1

  end subroutine assertFailed


  !> Stops code signalizing internal error.
  !!
  subroutine internalError(msg, file, line)
    character(*), intent(in) :: msg, file
    integer, intent(in) :: line

    write(stderr, "(A)") "!!! INTERNAL ERROR"
    write(stderr, "(A,1X,A)") "Msg: ", msg
    write(stderr, "(A,1X,A)") "File:", file
    write(stderr, "(A,1X,I0)") "Line:", line
    stop 2

  end subroutine internalError
    
end module libhsd_common
