include(common.m4)

!> Contains a set with pointer items.
!!
!! \todo Some more set operations (like union, section, etc.)
!!
module libhsd_ptrset
  use libhsd_common
  use libhsd_items
  use libhsd_ptrptrmap
  implicit none
  private

  public :: PtrSet, PtrSetView, PtrSetIterator, init
  

  !! Fake item type to be used for the underlying dictionary.
  type, extends(BaseItem) :: DummyItem
  contains
    procedure :: isEqualTo => DummyItem_isEqualTo
  end type DummyItem

  !! Fake item instance to be used for the underlying dictionary.
  type(DummyItem), target :: DUMMY_ITEM


  !> Implements a set of items.
  type :: PtrSet
    private
    type(PtrPtrMap) :: dict
    type(ItemPtr) :: pDummy
  contains
    procedure :: getSize
    procedure :: addPtr
    generic :: add => addPtr
    procedure :: deletePtr
    generic :: delete => deletePtr
    procedure :: hasPtr
    generic :: has => hasPtr
    procedure :: updateFromSet
    procedure :: updateFromSetView
    generic :: update => updateFromSet, updateFromSetView
    procedure :: isEqualToSet
    generic :: operator(==) => isEqualToSet
    procedure :: isNotEqualToSet
    generic :: operator(/=) => isNotEqualToSet
    procedure :: assignSet
    generic :: assignment(=) => assignSet
  end type PtrSet

  !> Structure constructor.
  interface PtrSet
    module procedure PtrSet_construct
  end interface PtrSet

  !> Initializer.
  interface init
    module procedure PtrSet_init
  end interface init

  
  !> Read-only view for a set
  type :: PtrSetView
    private
    class(PtrSet), pointer :: set
  contains
    procedure :: getSize => PtrSetView_getSize
    procedure :: hasPtr => PtrSetView_hasPtr
    generic :: has => hasPtr
  end type PtrSetView

  !> Structure constructor for PtrSetView
  interface PtrSetView
    module procedure PtrSetView_fromSet
  end interface PtrSetView


  !> Set iterator.
  type :: PtrSetIterator
    private
    type(PtrPtrMapIterator) :: dictIterator
  contains
    procedure :: getNextPtr => PtrSetIterator_getNextPtr
    generic :: getNext => getNextPtr
  end type PtrSetIterator

  !> Structure constructor for set iterator.
  interface PtrSetIterator
    module procedure PtrSetIterator_fromSet
    module procedure PtrSetIterator_fromSetView
  end interface PtrSetIterator
    
contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  PtrSet
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> Initializer for PtrSet.
  !!
  !! \param itemOwner  Whether set is responsible for destruction of its items.
  !!     
  subroutine PtrSet_init(this, minSize, itemOwner)
    type(PtrSet), intent(out) :: this
    integer, intent(in), optional :: minSize
    logical, intent(in), optional :: itemOwner

    call init(this%dict, minSize=minSize, keyOwner=itemOwner, itemOwner=.false.)
    this%pDummy%ptr => DUMMY_ITEM
    
  end subroutine PtrSet_init


  !> Structure constructor for PtrSet.
  !!
  !! \param itemOwner  Whether set is responsible for destruction of its items.
  !!     
  function PtrSet_construct(minSize, itemOwner) result(this)
    integer, intent(in), optional :: minSize
    logical, intent(in), optional :: itemOwner
    type(PtrSet) :: this

    call init(this, minSize=minSize, itemOwner=itemOwner)

  end function PtrSet_construct


  !> Determines the nr. of elements in the set.
  !!
  !! \return Nr. of elements.
  !!
  function getSize(this)
    class(PtrSet), intent(in) :: this
    integer :: getSize

    getSize = this%dict%getSize()

  end function getSize


  !> Adds a new item to the set.
  !!
  !! \param item  Item to be added.
  !!
  subroutine addPtr(this, item)
    class(PtrSet), intent(inout) :: this
    type(HashedItemPtr), intent(in) :: item

    call this%dict%set(item, this%pDummy)

  end subroutine addPtr


  !> Deletes an item from the set.
  !!
  !! \param item  Item to be deleted.
  !!
  subroutine deletePtr(this, item)
    class(PtrSet), intent(inout) :: this
    type(HashedItemPtr), intent(in) :: item

    call this%dict%delete(item)

  end subroutine deletePtr


  !> Checks, whether the set contains a certain item.
  !!
  !! \return  True if the item is in the set, false if not.
  !!
  function hasPtr(this, item) result(has)
    class(PtrSet), intent(in) :: this
    type(HashedItemPtr), intent(in) :: item
    logical :: has

    has = this%dict%hasKey(item)

  end function hasPtr


  !> Updates the set from an other one.
  !!
  !! \param other  Set to take the additional elements from.
  !!
  subroutine updateFromSet(this, other)
    class(PtrSet), intent(inout) :: this
    class(PtrSet), intent(in) :: other

    call this%dict%update(other%dict)

  end subroutine updateFromSet


  !> Updates the set from a view over an other one.
  !!
  !! \param other  Set to take the additional elements from.
  !!
  subroutine updateFromSetView(this, other)
    class(PtrSet), intent(inout) :: this
    class(PtrSetView), intent(in) :: other

    call this%dict%update(other%set%dict)

  end subroutine updateFromSetView


  !> Overwritten == operator for sets.
  !!
  function isEqualToSet(this, other) result(isEqual)
    class(PtrSet), intent(in) :: this, other
    logical :: isEqual

    isEqual = (this%dict == other%dict)

  end function isEqualToSet

  
  !> Overwritten /= operator for sets.
  !!
  function isNotEqualToSet(this, other) result(isNotEqual)
    class(PtrSet), intent(in) :: this, other
    logical :: isNotEqual

    isNotEqual = (.not. (this == other))
    
  end function isNotEqualToSet


  !> Overwritten assignment operator for sets.
  !!
  subroutine assignSet(this, other)
    class(PtrSet), intent(inout) :: this
    class(PtrSet), intent(in) :: other

    this%dict = other%dict
    this%pDummy = other%pDummy

  end subroutine assignSet


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! PtrSetView
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> Constructor for PtrSetView.
  !!
  !! \param set  Setionary to create a view for.
  !! \return  Initalized viewer instance.
  !!
  function PtrSetView_fromSet(set) result(this)
    class(PtrSet), target, intent(in) :: set
    type(PtrSetView) :: this

    this%set => set
    
  end function PtrSetView_fromSet


  !> Returns the number of key/value pairs stored in the setionary.
  !!
  function PtrSetView_getSize(this) result(getSize)
    class(PtrSetView), intent(in) :: this
    integer :: getSize

    getSize = this%set%getSize()

  end function PtrSetView_getSize


  !> Checks, whether a key is present in the setionary.
  !!
  !! \param key  Key to look for.
  !! \return True if the key is present, false if not.
  !!
  function PtrSetView_hasPtr(this, item) result(has)
    class(PtrSetView), intent(in) :: this
    type(HashedItemPtr), intent(in) :: item
    logical :: has

    has = this%set%has(item)

  end function PtrSetView_hasPtr


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  PtrSetIterator
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> Structure constructor for PtrSetIterator.
  !!
  !! \param set  Set to iterate over.
  !! \return  Initialized iterator.
  !!
  function PtrSetIterator_fromSet(set) result(this)
    class(PtrSet), target, intent(in) :: set
    type(PtrSetIterator) :: this

    this%dictIterator = PtrPtrMapIterator(set%dict)

  end function PtrSetIterator_fromSet


  !> Structure constructor for PtrSetIterator.
  !!
  !! \param set  Set to iterate over.
  !! \return  Initialized iterator.
  !!
  function PtrSetIterator_fromSetView(view) result(this)
    class(PtrSetView), target, intent(in) :: view
    type(PtrSetIterator) :: this

    this = PtrSetIterator(view%set)

  end function PtrSetIterator_fromSetView
  

  !> Returns next item in the set.
  !!
  !! \param item  Next item. If no more items are left, the pointer in
  !!     item is unassociated.
  !!
  subroutine PtrSetIterator_getNextPtr(this, item)
    class(PtrSetIterator), intent(inout) :: this
    type(HashedItemPtr), intent(out) :: item

    type(ItemPtr) :: dummy

    call this%dictIterator%getNext(item, dummy)

  end subroutine PtrSetIterator_getNextPtr


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  DummyItem
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  !! Overwritten == operator for DummyItem.
  !!
  !! \details Delivers always true, so that comparing dictionaries with
  !!     this items is reduced to a comparison of the keys.
  !!
  function DummyItem_isEqualTo(this, other) result(isEqual)
    class(DummyItem), intent(in) :: this
    class(BaseItem), intent(in) :: other
    logical :: isEqual

    isEqual = .true.

  end function DummyItem_isEqualTo

end module libhsd_ptrset
